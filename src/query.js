class Query {
	constructor (query = []) {
		this.query = query;
	}

	_add (property, operator, value) {
		if (this.query.length === 0) {
			this.query.push([]);
		}

		const current = this.query[this.query.length > 0 ? this.query.length - 1 : 0 ];
		const format = `${operator}${value}`;

		if (current[property] instanceof Array) {
			const set = new Set([...current[property]]);
			set.add(format);
			current[property] = [...set];
		} else {
			current[property] = [format];
		}
	}

	or () {
		this.query.push({});
		return this;
	}

	equals (property, value) {
		this._add(property, '=', value);
		return this;
	}

	notEquals (property, value) {
		this._add(property, '!', value);
		return this;
	}

	contains (property, value) {
		this._add(property, '~', value);
		return this;
	}

	doesNotContain (property, value) {
		this._add(property, '^', value);
		return this;
	}

	moreThan (property, value) {
		this._add(property, '>', value);
		return this;
	}

	lessThan (property, value) {
		this._add(property, '<', value);
		return this;
	}

	object () {
		return this.query;
	}

	remove (index, property, operator, value) {
		const current = this.query[index];

		if (current[property] instanceof Array) {
			current[property] = current[property].filter(c => c !== `${operator}${value}`);

			if (current[property].length === 0) {
				delete current[property];

				if (Object.keys(current).length === 0) {
					this.query.splice(index, 1);
				}
			}
		}
	}


	matches (object) {
		for (const querySet of this.query) {
			let matchesSet = true;

			for (const [key, conditions] of Object.entries(querySet)) {
				const real = object[key];

				let matchesConditions = true;

				for (const condition of conditions) {
					const operator = condition[0];
					const expected = condition.slice(1);

					if (typeof real === 'undefined') {
						matchesConditions = false;
					} else {
						if (operator === '=') {
							if (real.toString() !== expected) {
								matchesConditions = false;
							}
						} else if (operator === '!') {
							if (real.toString() === expected) {
								matchesConditions = false;
							}
						} else if (operator === '>') {
							if (Number(real) <= Number(expected)) {
								matchesConditions = false;
							}
						} else if (operator === '<') {
							if (Number(real) >= Number(expected)) {
								matchesConditions = false;
							}
						} else if (operator === '~') {
							const probableNumber = Number(real);
							if (isNaN(probableNumber)) {
								if (real.indexOf(expected) === -1) {
									matchesConditions = false;
								}
							} else {
								if (real.toString() !== expected) {
									matchesConditions = false;
								}
							}
						} else if (operator === '^') {
							const probableNumber = Number(real);
							if (isNaN(probableNumber)) {
								if (real.indexOf(expected) > -1) {
									matchesConditions = false;
								}
							} else {
								if (real.toString() === expected) {
									matchesConditions = false;
								}
							}
						}
					}
				}

				if (matchesConditions === false) {
					matchesSet = false;
					continue;
				}
			}

			if (matchesSet === true) {
				return true;
			}
		}

		return false;
	}
}

export default Query;