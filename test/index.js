const { Query } = require('../dist/want.node');

const query = new Query();

console.log(query.object());
query.equals('first_name', 'Jane')
	.notEquals('first_name', 'John')
	.contains('last_name', 'Doe')
	.or()
	.moreThan('age', 20);

console.log(query.object());

console.log('Matches:', query.matches({
	first_name: 'John',
	last_name: 'Doe'
}));

query.remove(0, 'first_name', '=', 'Diego');

console.log('Matches:', query.matches({
	first_name: 'Jane',
	age: 21
}));

console.log(query.object());

query.remove(0, 'first_name', '!', 'John');

console.log(query.object());

query.remove(0, 'last_name', '~', 'Doe');

console.log(query.object());

query.remove(0, 'age', '>', '20');

console.log(query.object());