# WANT Query Format

## Description

WANT is an acronym for We Are Not Tableau. It stands for an overly simplistic way to perform complex queries using an easy to parse format.

## Operators

WANT defines a set of universal operators to perform the queries.

| Operator | Logical Translation | Additional Notes and Behavior |
| :--- | :--- | :--- |
| `=` | Is equal |  |
| `!` | Is not equal |  |
| `~` | Contains | On numeric values, this operator will act the same as the `=` operator. |
| `^` | Does not contains | On numeric values, this operator will act the same as the `!` operator. |
| `>` | More than |  |
| `<` | Less than |  |

## Samples


All users whose [first name equals Jane `and` does not equal John `and` last name `contains` Doe] `OR` [age is `more than` 20]
```js
[
  [ first_name: [ '=Jane', '!John' ], last_name: [ '~Doe' ] ],
  { age: [ '>20' ] }
]
```

## License
WANT is a Free Open Source Software project released under the MIT License.